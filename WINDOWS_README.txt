dbxfs
=====

This is the binary distribution of dbxfs for Windows. It is
distributed as a portable executable, it doesn't need to be installed
to "Program Files." You can run it from anywhere.

dbxfs uses PyInstaller to package everything into a single executable.
Unfortunately lots of viruses also use PyInstaller to distribute
themselves.  This causes many anti-virus programs to incorrectly flag
dbxfs as a virus, including Windows Defender.  It is not a virus, you
may safely ignore any warning about it being a virus.  To bypass
Windows Defender warnings, click the "More info" link when you see the
"Windows protected your PC" dialog box after running dbxfs. From there
you can tell Windows to run the program anyway.

dbxfs is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

dbxfs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

A copy of the dbxfs source code is available at
https://thelig.ht/code/dbxfs/. You may contact the author on Twitter
at https://twitter.com/cejetvole. Enjoy.

Rian Hunter
