dbxfs
=====

**dbxfs is not licensed to be hosted on GitHub in response to GitHub's
history of exploitative behavior. If you are reading this on GitHub
this is an illegal mirror.  Do not use and report usage to the author
https://twitter.com/cejetvole**

dbxfs allows you to mount your Dropbox folder as if it were a local
filesystem. It differs from the official Dropbox client in two main
ways:

* Internet connectivity is required for access
* No disk space is required for access, but will cache if disk space is available

dbxfs has been tested on OpenBSD, Linux, macOS, and Windows but it should run on any
POSIX system that provides a FUSE-compatible library or has the
ability to mount SMB shares. It
runs on non-x86 architectures like ARM. It doesn't require a specific
file system.

It is written for Python 3.11+ and is licensed under the GPLv3.

Disclaimer: dbxfs is not affiliated with Dropbox, Inc.

Installation
------------

If you are on Windows, the easiest way to use dbxfs is to download and
run [the pre-compiled
executable](https://www.dropbox.com/scl/fo/5bjakuwingc6vk9kcgcy0/h?rlkey=bgab24afqq8rgb78qo9hwsum8&dl=0).
Read the README included with the Windows distribution for
Windows-specific instructions.

If you are on Linux, you must install your OS's FUSE library. On
Debian/Ubuntu, install the `libfuse2` package, on Fedora install
`fuse`.

Run the following command:

    $ pip3 install dbxfs

On Arch Linux and derivatives, you can find it in the AUR as
[dbxfs](https://aur.archlinux.org/packages/dbxfs).

Usage
-----

Use `dbxfs` like you would use the `mount` command:

    $ dbxfs <mount_point>

To unmount your Dropbox folder on Linux systems, you can use
`fusermount -u <mount_point>`, on all other systems use `umount`.

You can see the full list of command line options by passing `-h` to
the `dbxfs` command.

Advanced Refresh Token Storage
------------------------------

By default dbxfs stores your refresh token in an
encrypted file but you may want to store it in a GPG encrypted file
or something else. To do that you must first obtain a refresh token.
You can obtain a refresh token by running the following command:

    $ dbxfs --get-refresh-token

Once you have obtained the refresh token, encrypt it with the program of
your choice and store the result somewhere. After that, you must edit
the dbxfs config file. You can find the location of the config file by
running the following command:

    $ dbxfs --print-default-config-file

The config file is a JSON encoded file. Add the following JSON key to
the top-level JSON object in that file:

    "refresh_token_command": ["gpg", "--decrypt", "/path/to/refresh/token/file.gpg"]

Adapt it to a decryption program of your choice. This configuration
works great for storing the refresh token using a OpenPGP card.

Contact
-------

Rian Hunter [@cejetvole](https://twitter.com/cejetvole)
